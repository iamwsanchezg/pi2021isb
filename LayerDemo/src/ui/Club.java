package ui;


import java.util.ArrayList;
import java.util.List;

import ol.Person;

public class Club {
	private String ClubName;
	private List<Person> Members;
	
	public Club(String clubName) {
		super();
		ClubName = clubName;
		Members = new ArrayList<Person>();
	}

	public Club(String clubName, List<Person> members) {
		super();
		ClubName = clubName;
		Members = members;
	}
	
	public void addMember(Person member) {
		Members.add(member);
	}
	public void showClub() {
		System.out.println("===================== " + ClubName + " =====================");
		for(int i=0;i<Members.size();i++) {
			System.out.println(Members.get(i).getFirstName() + " " + Members.get(i).getLastName());
		}
	}
}
