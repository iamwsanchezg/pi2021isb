package ui;

import java.util.ArrayList;
import java.util.List;

import ol.Professor;
import ol.Student;

public class ClassGroup {
	protected Professor instructor;
	protected List<Student> students;
	public ClassGroup() {
		super();
		students = new ArrayList<Student>();
	}
	public Professor getInstructor() {
		return instructor;
	}
	public void setInstructor(Professor instructor) {
		this.instructor = instructor;
	}
	public List<Student> getStudents() {
		return students;
	}
	public void setStudents(List<Student> students) {
		this.students = students;
	}
	public void addStudent(Student student) {
		students.add(student);
	}
	public void showGroup() {
		System.out.println("===================== INSTRUCTOR: " + instructor.getFirstName() + " " + instructor.getLastName() + " =====================");
		for(int i=0;i<students.size();i++) {
			System.out.println("Nombre: " + students.get(i).getFirstName() + " " + students.get(i).getLastName() + ", Nivel: " + students.get(i).getLevel());
		}
	}
}
