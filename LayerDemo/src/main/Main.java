package main;

import ol.Person;
import ol.Professor;
import ol.Student;
import ui.ClassGroup;
import ui.Club;

public class Main {

	public static void main(String[] args) {
		
		Club myClub = new Club("Promecys ISB 2021");
		myClub.addMember(new Person("William","Sanchez"));
		myClub.addMember(new Person("Rebeca","Almeida"));
		myClub.showClub();
		
		
		System.out.println("********************************************************");
		ClassGroup group = new ClassGroup();
		group.setInstructor(new Professor("Alba","Cerda"));
		Student cesar = new Student("Cesar","Trillo");
		cesar.setLevel(1);
		group.addStudent(cesar);
		Student neyli = new Student("Neyli","Madriz");
		neyli.setLevel(5);
		group.addStudent(neyli);
		group.showGroup();
	}

}
